import { Component, Input, OnInit } from '@angular/core';
import { IMahjong } from '../../models/mahjong';
import { randomPrimeNumbers, shuffle } from '../../utils';
import { interval, of, Subscription } from 'rxjs';
import { delay, switchMap, take, tap } from 'rxjs/operators';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {
  public selectedMahjongItem: IMahjong;
  public gameItems: IMahjong[] = [];
  private _gameSize: number;

  @Input('gameSize')
  private get gameSize(): number {
    return this._gameSize;
  }
  private set gameSize(gameSize: number) {
    this._gameSize = gameSize;

    const primeNumbers = randomPrimeNumbers(Math.floor(gameSize / 2));

    this.gameItems = shuffle(
      primeNumbers
        .concat(primeNumbers)
        .map((value: number, index: number): IMahjong => {
          return {
            id: value,
            isVisible: false,
            isPair: false,
            uniqueId: 10000 + index
          }
        })
    );

    console.table(this.gameItems.map((value: IMahjong, index) => {
      return {
        index: index + 1,
        value: value.id
      }
    }));
  }

  constructor() {}

  public selectedItem(mahjongItem: IMahjong): void {
    if (this.selectedMahjongItem) {
      if (this.selectedMahjongItem.uniqueId === mahjongItem.uniqueId) return;

      if (this.selectedMahjongItem.id === mahjongItem.id) {
        this.selectedMahjongItem.isVisible = mahjongItem.isVisible = true;
        this.selectedMahjongItem.isPair = mahjongItem.isPair = true;
        this.selectedMahjongItem = null;

        if (this.detectIsEndGame()) {
          const subscription: Subscription = interval(1000)
            .pipe(
              take(5),
              switchMap(() => of(null).pipe(
                tap(() => this.actionNewGame())
              ))
            ).subscribe((): void => subscription.unsubscribe());
        }
      } else {
        mahjongItem.isVisible = true;

        const subscription: Subscription = of(null)
          .pipe(
            delay(1000),
            tap((): void => {
              this.selectedMahjongItem.isVisible = mahjongItem.isVisible = false;
              this.selectedMahjongItem = null;
            })
          ).subscribe((): void => subscription.unsubscribe());
      }
      return;
    }

    this.selectedMahjongItem = mahjongItem;
    mahjongItem.isVisible = true;
  }

  private detectIsEndGame(): boolean {
    return this.gameItems.filter((gameItem: IMahjong) => gameItem.isVisible).length === this.gameSize;
  }

  private actionNewGame(): void {
    this.gameSize = 30;
  }

  ngOnInit(): void {}
}
