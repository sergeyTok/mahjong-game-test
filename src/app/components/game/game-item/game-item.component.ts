import { Component, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';
import { IMahjong } from '../../../models/mahjong';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-game-item',
  templateUrl: './game-item.component.html',
  styleUrls: ['./game-item.component.scss'],
  animations: [
    trigger('isVisibleNumber',[
      state('visible', style({
        opacity: 1
      })),
      state('unVisible', style({
        opacity: 0
      })),
      transition('visible => unVisible', [
        animate('1s')
      ]),
      transition('unVisible => visible', [
        animate('0.5s')
      ])
    ])
  ]
})
export class GameItemComponent implements OnInit {
  @HostListener('click') onClick() {
    this.selectItemOutput.next(this.mahjongItem);
  }

  @Input() public mahjongItem: IMahjong;
  @Output() public selectItemOutput: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {}
}
