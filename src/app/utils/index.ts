function arrayPrimeNumbers(start: number, end: number) {
  return Array(end - start + 1)
    .fill('')
    .map((_, index: number) => start + index)
    .filter((value: number) => {
      for(let i = 2; i < value; i++)
        if(value % i === 0) return false;
      return value > 1;
    });
}

export function randomPrimeNumbers(endBorder: number) {
  let maxPrimeBorder = 50;
  let arrPrimeNumbers: number[] = arrayPrimeNumbers(1, maxPrimeBorder);

  while (arrPrimeNumbers.length < endBorder) {
    arrPrimeNumbers = arrPrimeNumbers.concat(arrayPrimeNumbers(maxPrimeBorder, maxPrimeBorder += 50));
  }

  return arrPrimeNumbers.slice(0, endBorder);
}

export function shuffle(array: any[]) {
  let currentIndex =
    array.length,
    tempValue,
    randomIndex;

  while (currentIndex != 0) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    tempValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = tempValue;
  }

  return array;
}
