export interface IMahjong {
  uniqueId: number;
  id: number;
  isVisible: boolean;
  isPair: boolean;
}
